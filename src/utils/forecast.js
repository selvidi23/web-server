const request = require('request')

const forecast = (latitude, longlatitude , callback) => {
    
    const url = 'https://api.darksky.net/forecast/d088506d5135a816fdcde9c286cb72f3/'+ encodeURIComponent(latitude) +',' + encodeURIComponent(longlatitude)+'?units=si'

    request ({url, json: true},(error,{body}) => {
        if(error){
            callback('Unable to connect to weather service!', undefined)
        }else if (body.error){
            callback('Unable to find loction', undefined)
        }else {
            callback(undefined, body.daily.data[0].summary + ' It is currently ' + body.currently.temperature +' degress out.' + ' There is a ' + body.currently.precipProbability + '% chance of rain. ' + body.timezone)
        }
    })
}

module.exports = forecast