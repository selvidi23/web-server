const path = require('path')
const express = require('express')
const hbs = require('hbs')

const app = express()

//Paths for Express config
const publicDirectoryPath = path.join(__dirname, '../public')
const viewsPath = path.join(__dirname,'../templates/views')
const partialsPath = path.join(__dirname,'../templates/partials')

app.set('view engine','hbs')
app.set('views',viewsPath)
hbs.registerPartials(partialsPath)


app.use(express.static(publicDirectoryPath))

app.get('',(req,res) => {
    res.render('index',{
        title:'Weather app',
        name: 'Bob'
    })
})

app.get('/about',(req,res) => {
    res.render('about',{
        title:'About',
        name: 'Bob'
    })
})

app.get('/help',(req,res) => {
    res.render('help',{
        helpText: 'Help text ',
        title: 'Help',
        name: 'Bob '
    })
})


app.get('/weather', (req, res) => {
    if (!req.query.address) {
        return res.send({
            error: 'You must povide address!',
        })
    } 

    res.send({
        forecast: 'its snow',
        location: 'Warsaw',
        address: req.query.address
    })
})

app.get('/products', (req,res) => { 
    if (!req.query.search) {
        return res.send({
            error: 'You must provide a search term',
        })
    } 

    console.log(req.query.search)
    res.send({
        products: [],
    })

})

app.get('/help/*',(req,res) => {
    res.render('404',{
        title: '404',
        errorMessage: 'Help article not found.'
    })
})

app.get('*',(req, res) => {
    res.render('404', {
        title: '404',
        errorMessage: 'Page not found.'
    })
})

app.listen(3000,() =>{
    console.log('Server is uo on port 3000.')
})